#!/bin/sh

for i in $(seq 10); do
  if xsetwacom list devices | grep -q Wacom; then
    break
  fi
  sleep 1
done

stylus=$(xsetwacom list devices | awk '/stylus/{print $8}')
xsetwacom set "${stylus}" TabletPCButton on
