export TERM="screen-256color"
export DISPLAY=:0.0

source ~/.zshenv-local
source ~/.zshalias
export PATH="$PATH:$HOME/.scripts:$HOME/.local/bin:$HOME/bin"

export EDITOR="vim"
export MORE="-s"
export PAGER="less"
export MANPAGER="$PAGER"
export VISUAL="$EDITOR"
export HOST="$HOST"

fpath=($fpath $HOME/.zsh/func)
typeset -U fpath
