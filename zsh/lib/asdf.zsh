#!/usr/bin/env zsh

#
# zsh-asdf
#
# version: 0.0.1
# author: Lonnon Foster
#

asdf_setup() {
  # Find where asdf should be installed
  ASDF_DIR="${ASDF_DIR:-$HOME/.asdf}"
  ASDF_COMPLETIONS="$ASDF_DIR/completions"

  # Load command
  if [[ -f "$ASDF_DIR/asdf.sh" ]]; then
      . "$ASDF_DIR/asdf.sh"

      # Load completions
      fpath=($fpath ${ASDF_DIR}/completions)
      autoload -Uz compinit
      compinit
  fi
}

asdf_setup "$@"
